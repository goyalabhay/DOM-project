const fruits = [
  "🍇",
  "🍇",
  "🍒",
  "🍒",
  "🍉",
  "🍉",
  "🍈",
  "🍈",
  "🍊",
  "🍊",
  "🍌",
  "🍌",
  "🍍",
  "🍍",
  "🥭",
  "🥭",
];
let movesCount = 0;
let firstClick = false;
let gameOver = false;
let isStarted = false;
let hasFlipped = false;
let firstCard, secondCard;
let openedCards = 0;
let time = 0;

const timer = document.querySelector(".timer");
const moves = document.querySelector(".moves");
const start = document.querySelector(".start");
const cards = document.querySelectorAll(".box");
const container = document.querySelector(".container");

fruits.sort(() => Math.random() - 0.5);

cards.forEach((card, index) => {
  card.querySelector(".back").querySelector(".fruit").innerHTML = fruits[index];
});

start.addEventListener("click", started);

cards.forEach((card) => card.addEventListener("click", flip));

function started() {
  if (!isStarted) {
    isStarted = true;
    cards.forEach((card) => card.classList.toggle("flip"));
    setTimeout(() => {
      cards.forEach((card) => card.classList.toggle("flip"));
    }, 1000);
  }
}

function flip() {
  if (isStarted) {
    this.classList.add("flip");
    movesCount++;
    moves.innerText = `${movesCount} moves`;
    if (!firstClick) {
      startTimer();
      firstClick = true;
    }

    if (!hasFlipped) {
      hasFlipped = true;
      firstCard = this;
    } else {
      hasFlipped = false;
      secondCard = this;
      checkForMatch();
    }
  }
}

function checkForMatch() {
  if (
    firstCard.querySelector(".back").querySelector(".fruit").innerHTML ===
    secondCard.querySelector(".back").querySelector(".fruit").innerHTML
  ) {
    disableCards();
  } else {
    unflip();
  }
}

function disableCards() {
  firstCard.removeEventListener("click", flip);
  firstCard.removeEventListener("click", flip);
  openedCards++;
  if (openedCards === 8) {
    gameOver = true;
    // alert("Gluten");
  }
}

function unflip() {
  setTimeout(() => {
    firstCard.classList.remove("flip");
    secondCard.classList.remove("flip");
  }, 400);
}

function startTimer() {
  if (isStarted) {
    setInterval(() => {
      time++;
      if (!gameOver) {
        timer.innerText = `Time: ${time} sec`;
      } else {
        cards.forEach((card) => card.removeEventListener("click", flip));
        container.innerText = "Congratulations";
        container.style.fontSize = "50px";
      }
    }, 1000);
  }
}
